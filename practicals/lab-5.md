# Lab 5

 The purpose of this lab is to get students to design and build a distributed system following a micro-service architectural style. Here, we focus on problem decomposition using different techniques, including domain-driven (DDD) design and verb and noun decomposition with a RESTful API style. We also look into how to design and implement an API gateway and support for indirect communication between services.

## Problem 

Consider a virtual learning environment application that provides students with access to learning materials in various formats (slides, lecture notes, audio and video content) for each topic taught within a given course (e.g., DSP620S). The system also supports communication between different types of users in the system (students, lecturers, content producers). Finally, students can take an assessment prepared by a lecturer.

Your task is to:

1. using different approaches, propose a decomposition of the problem into micro-services;
2. deploy and set up a Kafka cluster to support communication between the micro-services;
3. implement your micro-services in Ballerina, deploy them in Docker and Kubernetes and test them;
4. Build an API gateway that captures all requests/calls to the system before dispatching them to the micro-service. Choose [graphql](https://graphql.org/) as your API query language
5. Propose tools to monitor the system