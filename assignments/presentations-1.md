DSP620S Assignment 1 Presentation Schedule
===========================================

In this document, we compile the presentation notes for each group.

# Presentation time slots

| Date | Time slot | Group |
|------|-----------|-------|
|09/12/2020 | 11:15 - 11:35 | Group 17 |
|11/12/2020 | 11:00 - 11:20 | Group 24 |
| | | |
|01/02/2021 | 09:00 - 09:20 |  |
|01/02/2021 | 09:20 - 09:40 |  |
|01/02/2021 | 09:40 - 10:00 |  |
|01/02/2021 | 10:00 - 10:20 |  |
|01/02/2021 | 10:20 - 10:40 |  |
|01/02/2021 | 10:40 - 11:00 |  |
|01/02/2021 | 11:00 - 11:20 |  |
|01/02/2021 | 11:20 - 11:40 |  |
|01/02/2021 | 11:40 - 12:00 |  |
|01/02/2021 | 12:00 - 12:20 |  |
|01/02/2021 | 12:20 - 12:40 |  |
|01/02/2021 | 12:40 - 13:00 |  |
|01/02/2021 | 17:00 - 17:20 |  |
|01/02/2021 | 17:20 - 17:40 |  |
|01/02/2021 | 17:40 - 18:00 |  |
|01/02/2021 | 18:00 - 18:20 |  |
|01/02/2021 | 18:20 - 18:40 |  |
|01/02/2021 | 18:40 - 19:00 |  |
|02/02/2021 | 09:00 - 09:20 |  |
|02/02/2021 | 09:20 - 09:40 |  |
|02/02/2021 | 09:40 - 10:00 |  |
|02/02/2021 | 10:00 - 10:20 |  |
|02/02/2021 | 10:20 - 10:40 |  |
|02/02/2021 | 10:40 - 11:00 |  |
|02/02/2021 | 11:00 - 11:20 |  |
|02/02/2021 | 11:20 - 11:40 |  |
|02/02/2021 | 11:40 - 12:00 |  |
|02/02/2021 | 12:00 - 12:20 |  |
|02/02/2021 | 12:20 - 12:40 |  |
|02/02/2021 | 12:40 - 13:00 |  |
|03/02/2021 | 09:00 - 09:20 |  |
|03/02/2021 | 09:20 - 09:40 |  |
|03/02/2021 | 09:40 - 10:00 |  |
|03/02/2021 | 10:00 - 10:20 |  |
|03/02/2021 | 10:20 - 10:40 |  |
|03/02/2021 | 10:40 - 11:00 | Group 4 |
|03/02/2021 | 11:00 - 11:20 | Group 14 |
|03/02/2021 | 11:20 - 11:40 |  |
|03/02/2021 | 11:40 - 12:00 |  |
|03/02/2021 | 12:00 - 12:20 |  |
|03/02/2021 | 12:20 - 12:40 |  |
|03/02/2021 | 12:40 - 13:00 |  |
|03/02/2021 | 17:00 - 17:20 |  |
|03/02/2021 | 17:20 - 17:40 |  |
|03/02/2021 | 17:40 - 18:00 |  |
|03/02/2021 | 18:00 - 18:20 |  |
|03/02/2021 | 18:20 - 18:40 |  |
|03/02/2021 | 18:40 - 19:00 | Group 12 |
|04/02/2021 | 09:00 - 09:20 |  |
|04/02/2021 | 09:20 - 09:40 |  |
|04/02/2021 | 09:40 - 10:00 | Group 19 |
|04/02/2021 | 10:00 - 10:20 |  |
|04/02/2021 | 10:20 - 10:40 |  |
|04/02/2021 | 10:40 - 11:00 |  |
|04/02/2021 | 11:00 - 11:20 |  |
|04/02/2021 | 11:20 - 11:40 |  |
|04/02/2021 | 11:40 - 12:00 |  |
|04/02/2021 | 12:00 - 12:20 | Group 13 |
|04/02/2021 | 12:20 - 12:40 |  |
|04/02/2021 | 12:40 - 13:00 |  |
|04/02/2021 | 17:00 - 17:20 | Group 10 |
|04/02/2021 | 17:20 - 17:40 |  |
|04/02/2021 | 17:40 - 18:00 |  |
|04/02/2021 | 18:00 - 18:20 | Group 8 |
|04/02/2021 | 18:20 - 18:40 | Group 25 |
|04/02/2021 | 18:40 - 19:00 | Group 11 |
|05/02/2021 | 09:00 - 09:20 | Group 5 |
|05/02/2021 | 09:20 - 09:40 |  |
|05/02/2021 | 09:40 - 10:00 |  |
|05/02/2021 | 10:00 - 10:20 | Group 18 |
|05/02/2021 | 10:20 - 10:40 |  |
|05/02/2021 | 10:40 - 11:00 |  |
|05/02/2021 | 11:00 - 11:20 | Group 16 |
|05/02/2021 | 11:20 - 11:40 |  |
|05/02/2021 | 11:40 - 12:00 |  |
|05/02/2021 | 12:00 - 12:20 |  |
|05/02/2021 | 12:20 - 12:40 |  |
|05/02/2021 | 12:40 - 13:00 | Group 7 |
|05/02/2021 | 17:20 - 17:40 | Group 1 |
|09/02/2021 | 11:00 - 11:20 | Group 2 |
|09/02/2021 | 16:40 - 17:00 | Group 9 |
|10/02/2021 | 16:40 - 17:00 | Group 7 |
|11/02/2021 | 17:20 - 17:40 | Group 27 |
|12/02/2021 | 10:00 - 10:20 | Group 2 |
|12/02/2021 | 10:40 - 11:00 | Group 29 |
|12/02/2021 | 11:00 - 11:20 | Group 28 |
|12/02/2021 | 11:20 - 11:40 | Group 23 |
|12/02/2021 | 11:40 - 12:00 | Group 30 |
|12/02/2021 | 12:20 - 12:40 | Group 31 |
|12/02/2021 | 12:40 - 13:00 | Group 34 |
|15/02/2021 | 9:20 - 09:40 | Group 32 |
|15/02/2021 | 10:00 -10:20 | Group 33 |




# Evaluation criteria

The following criteria will be followed to assess each submission

* Definition of the remote interface in **Protocol Buffer**.
* Implementation of the gRPC client and server in the **Ballerina language**.
* Implementation of the server-side logic in response to the remote invocations, including the persistent storage and versioning.


# Notes on group presentations

## Group 1

- Repository: [Submission](https://github.com/Sacky061/DSP-first-project.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Shifidi Metumo N | 219056676 | FT |
| Kudumo A. K. | 217005195 | PT |
| Leornardo Petrus | 217070418 | FT |
| Simon S. S. | 213031272 | PT |

- Presentation
	- Date: 05/02/2021
	- Time: 17:20 - 17:40
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- remote interface
			- addRecord
			- updateRecord
			- readRecord (with key, key and version)
		- gRPC client
			- attempted to implement gRPC
		- gRPC server
			- attempted to implement gRPC
		- Server-side logic
			- used a json file to persist records
			- versioning not implemented
		- running program
			- partially
			- gRPC not working
- Mark:

## Group 2

- Repository: [Submission](https://gitlab.com/ronaldsangunji/dsp-project)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Nestor Nathingo | 219051844 | FT |
| Sakaria Shilikomwenyo | 219055572 | FT |
| Simeon Hifikepunye | 219047987 | FT |
| Ronald Sangunji | 217069649 | FT |

- Presentation
	- Date: 12/02/2021
	- Time: 10:00 - 10:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
			- 219051844 and 219047987 did not show up
		
		
		- remote interface
			- create and update
		- gRPC client
			- not working
		- gRPC server
			- compiles
		- Server-side logic
			- versioning: last child update
			- persistence: several files
		- running implementation: 
			- only compilation
- Mark:

## Group 3

- Repository: [Submission](https://gitlab.com/andsim/dsp.assignment01)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Andrew P. Ndapito |  |  |


- Presentation
	- Date:
	- Time:
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:

## Group 4

- Repository: [Submission](https://gitlab.com/makosaisaac/dspfirstasignment/-/tree/master)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Isaac Makosa | 219005516 | FT |
| Takunda Hwaire | 219155380 | FT |
| Tafadzwa Taringa | 219003068 | FT |

- Presentation
	- Date: 03/02/2021
	- Time: 10:40 - 11:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 22/11/2020
		
		
		- remote interface
 			- write function
			- update function
			- read with key
			- read with key and version
			- read with criteria (band name, date, ...)
		- gRPC client
			- All the functions are well implemented on the client except the read with criteria
			- The team explained well and pointed out the challenge they experienced to full implement the read with criteria function
		- gRPC server
			- The server implements all functions except the read with criteria
			- The team had some codes for the read with criteria but not fuctional
		- server-side logic
			- Well implemented except for the read with criteria function
			- The teams seems to have well understood the assignment and work hard
		- running program
		   - yes
- Mark:

## Group 5

- Repository: [Submission](https://github.com/gbananas190/DSP-assignment)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Gifty Muhinda | 217109187 | FT |
| Johannes Shishasha | 217056245 | FT |
| Olaf Benjamin | 218002343 | FT |


- Presentation
	- Date: 05/02/2021
	- Time: 09:00 - 09:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 22/11/2020
			- Johannes Shishasha was not available for Presentation
		- remote interface
			- no remote functions
			- proto buffer file is missing
			- no visible implementation of the gRPC
		- gRPC client
			- only one file 
			- there was no separate file for server and client
			- versioning method is implemented 
			- create a new record function is implemented in a just a normal ballerina file
			- The hashing function is implemented and called but no key is being generated
			- key is being generated randomly not using the hashing function
			- data is being saved to a text file
		- gRPC server
			- updating method implemented
			- search for a record with a key returns all records instead of the latest record
			- there are some implementation done that was apparently suppose to be incorperated on the server side
		- Server-side logic
			- everything is done in one file 
			- students explained some logic but there were no http or gRPC implementation
		- running program
		  - yes
- Mark:

## Group 6

- Repository: [Submission](https://github.com/Ashivudhi/DSP620S-Assignment-1)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Erro R. Ashivudhi | 216019362 | PT |
| David Ingashipola Andreas | 217111904 | FT |
| Joseph Nairenge | 211055832 | PT |
| Valombola Simon | 216022959 | PT |


- Presentation
	- Date:
	- Time:
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:

## Group 7

- Repository: [Submission](https://gitlab.com/dsp620s-2020-group_mmxx/dsp620s-assignment1)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Florencia Kauhanda | 214000273 | PT |
| Khoa Goodwill | 219080534 | PT |
| Ushi Mukaru | 20010475 | PT |
| Vulikeni Kashikuka | 200442368 | PT |


- Presentation
	- Date: 10/02/2021
	- Time: 16:20 - 17:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- remote interface
			- write
			- update
			- read (with key, with key and version, the criterion option was not clear)
		- gRPC client
			- implemented according to the remote interface
		- gRPC server
			- implemented according to the remote interface
		- Server-side logic
			- persistence: used MongnDB
			- versioning: only last child
		- running program
			- partially (the system kept freezing)
- Mark:

## Group 8

- Repository: [Submission](https://github.com/Oprah25/DSP-assignment.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Naemi N. Johannes | 214009804 |  |
| Kay-Lynne van Wyk | 219114056 |  |
| Rosvita Haikera | 214076008 |  |


- Presentation
	- Date: 04/02/2021
	- Time: 18:00 - 18:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- remote interface
			- addRecord
			- update
			- read (with key)
		- gRPC client
			- attempted to implement gRPC
		- gRPC server
			- attempted to implement gRPC
		- server-side logic
			- no persistence; records are only stored in memory
			- versioning not implemented
		- running program
			- No
- Mark:

## Group 9

- Repository: [Submission](https://github.com/)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Eliakim Haufiku | 215014928 | FT |
| Mwafangeyo Germias | 218060750 | FT |
| Hamushila Petrus | 218074336 | FT |
| Nghipandulwa Leonard | 218060343 | FT |


- Presentation
	- Date: 09/02/2021
	- Time: 16:40 - 17:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
			- the presenter disappeared while attempting to run the program and no other team member wished to continue
		
		
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
			- comment
		- running program
			- no/yes
- Mark:

## Group 10

- Repository: [Submission](https://github.com/)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Jimmy Sergio Damiao | 218090013 | PT |


- Presentation
	- Date: 04/02/2021
	- Time: 17:00 - 17:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- remote interface
			- add
			- update
			- read (with key)
		- gRPC client
			- the client implements all functions from the proto file
		- gRPC server
			- the server implements all remote functions in the client
		- Server-side logic
			- the records are stored in an in-memory map: no persistence of records
			- only uses the latest version of an object
			- implements CAS
		- running program
			- yes
- Mark:

## Group 11

- Repository: [Submission](https://gitlab.com/kanengonitinashe17/dsp-assignment-1)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Tinashe Kanengoni | 219034559 | PT |
| Antonio Reinecke | 219143218 | FT |
| Dionosius A. Beukes | 217099114 | FT |


- Presentation
	- Date: 04/02/2021
	- Time: 18:40 - 19:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 22/11/2020
			- Dionosius Beukes was not present for the presentation
			- Ony two members commited work to the repo 
			- Dionosius Beukes have not commited any changes to the repo
		
		
		- remote interface
			- insertRec function
			- updatingRec function
			- readRecordWithKey function
			- readRecordWithKeyVer function
			- readRecordWithCriterion function
		- gRPC client
			- All the functions have been well implemented except the updating functions that overwrites the exisiting file instead of creating a new separate file
			- According to the me the team well understood the project
			- observation is that more research was invested in the project and the team worked hard 
		- gRPC server
			- all the functions are well implemented except the updating file that overwrite the existing file
			- the hash function is well implemented
			- impressive work
		- Server-side logic
		 	- data is being saved to the json file
		 	- searching with criteria is well implemented
		- running program
		  - yes
- Mark:

## Group 12

- Repository: [Submission](https://github.com/218118740/Cali.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Silas T. Nghuushi | 218118740 | PT |
| Jesmine Cloete | 201032554 | PT |
| Erikson Abraham | 218062974 | PT |
| Sakeus Nangolo | 219150354 | PT |


- Presentation
	- Date: 03/02/2021
	- Time: 18:40 - 19:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- server-side logic
			- comment
		- running program
			- no/yes
- Mark:


## Group 13

- Repository: [Submission](https://github.com/oUchezuba/DSPassigment)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Owen Uchezuba | 219038309 | FT |
| De Almeida Reis L. R. | 219081662 | FT |
| Jurema Martins | 219060789 | FT |

- Presentation
	- Date: 04/02/2021
	- Time: 12:00 - 12:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 22/11/2020
			- all members contributed
		- remote interface
			- insert function
			- updatingRecord functionsreadRecordwithkey Function
		- gRPC client
			- Very minimum is done in the client
			- implementation of the functions from the prof file is wierd and different
			- no versioning
			- hardcoded records not the same as the one in the proto file
		- gRPC server
			- the implementation is different from what is expected from the assignment
			- effort to implement the insert function is done but not full implemented
			- no data is being saved to a json file
		- Server-side logic
		 - no logic
		- running program
		 - no
- Mark:

## Group 14

- Repository: [Submission](https://github.com/fadedphantom/DSP_Assignment)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Michael Apata | 219050449 |  |
| Matheus J. Tangeni | 214082474 |  |
| Nghidinwa N.V. Vatileni | 217090044 |  |
| Nghidinwa Jason P. G. Du Plessis | 217076459 |  |


- Presentation
	- Date: 03/02/2021
	- Time: 11:00 - 11:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 22/11/2020
		
		
		- remote interface
			- - write function
			- update function
			- Some other functions reply 1 to 5 dont make sense
		- gRPC client
			- Only the write and update functions have been implemented.
		- gRPC server
			- data is being saved to a JSon file
			- no versioning
			- generation of hashing key is working
			- only the write and update function is implemented
		- Server-side logic
		  	- very poor implementation
		- running program
		 - yes	  
- Mark:


## Group 15

Should mark this group as invalid and replace with 23

- Repository: [Submission](https://github.com/Selma-Auala/DSP-Assignment.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Lazarus Matheus | 219067023 |  |
| Stanza Narib | 217144810 |  |
| Selma Auala | 219009430 |  |
| Christoph Iitope | 217139485 |  |


- Presentation
	- Date: 12/02/2021
	- Time: 11:20 - 11:40
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:

## Group 16

- Repository: [Submission](https://github.com/WillyWonker69/DSP_ClassOf2020_ExclusiveIT)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Brave Wilson Kahweka | 219070318 | FT |
| Mino V. P. David | 219079846 | FT |
| Cesario D. C. Alves | 219150494 | FT |

- Presentation
	- Date: 05/02/2021
	- Time: 11:00 - 11:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- remote interface
			- insert
			- update
			- delete?
		- gRPC client
			- attempted to implement gRPC
		- gRPC server
			- attempted to implement gRPC
		- server-side logic
			- used a json file for persistence
			- only latest version
		- running program
			- No
- Mark:


## Group 17

- Repository: [Submission](https://github.com/kauly-dot-repo/DSP-Assignment)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Kaulyaalalwa Peter | 219143765 | FT |
| Oletu M. Shikomba | 219060355 | FT |
| Adilson D. Da Costa | 219020124 | FT |
| Emanuel A. Q. Vuma | 219091617 | FT |


- Presentation
	- Date: 09/12/2020
	- Time: 11:15 - 11:35
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 25/11/2020
			- all members contributed: yes
		
		
		- remote interface
			- write function
			- update function
			- read with key
			- read with key and version
			- read with criteria (band name, date, ...)
			- the objects in these functions are sometimes weird
		- gRPC client
			- the client implements all functions, except the read with criteria
			- the use cases are hardcoded
		- gRPC server
			- the server implements all the functions, but does not do streaming for the read with criteria
		- server-side logic
			- all records are persisted in a Mongodb instance
			- the version numbers are stored in the db instead of a separate implementation
		- running program
			- yes
- Mark:


## Group 18

- Repository: [Submission](https://github.com/VilhoHub/DSP-Assignment-/commits/main)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Vilho Nguno | 219068208 | FT |
| Vesta Ngomberume | 219085803 | FT |
| Vijanda V. Herunga | 216016681 | FT |
| Shania Nkusi | 219051704 | FT |


- Presentation
	- Date: 05/02/2021
	- Time: 10:00 - 10:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 22/11/2020
			- all members contributed
		- remote interface
			- write function
			- update function
			- readRecord function
		- gRPC client
			- all the functions in proto have been implemented satificatory 
			- no version
			- hashing function not implemented
			- data are hard coded and user cannot enter data
		- gRPC server
			- all functions in the proto have been implemented here
			- search for record was being done using the id 
		- Server-side logic
		  - data is being saved to a map
		- running program
		 - yes
- Mark:


## Group 19

- Repository: [Submission](https://gitlab.com/Lee439/dsp-assignment)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Lemuel Mayinoti | 219156786 | FT |
| LeeRoy Oliver | 218113951 | FT |


- Presentation
	- Date: 04/02/2021
	- Time: 09:40 - 10:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 22/11/2020
		
		
		- remote interface
			- insert
			- update
			- read (with key, with key and version)
		- gRPC client
			- attempted to implement gRPC
		- gRPC server
			- attempted to implement gRPC
		- server-side logic
			- used a json file for persistence
			- only latest version
		- running program
			- No (gRPC server)
- Mark:


## Group 20

- Repository: [Submission](https://gitlab.com/grouppros2020/dsp-assignment.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Helena Nambonga | 219063117 | FT |
| Hena Sibalatani | 219153566 | FT |
| Eugene Mwewa | 218040245 | FT |
| Imelda Haufiku | 219065128 | FT |


- Presentation
	- Date:
	- Time:
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
			- comment
		- running program
			- no/yes
- Mark:


## Group 21

- Repository: [Submission](https://github.com/ScientistFromIcarus/Assignment1)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Filemon Frans | 219000131 |  |
| Emma | 219067627 |  |
| Joel Paim | 218063067 |  |


- Presentation
	- Date:
	- Time:
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
			- comment
		- running program
			- no/yes
- Mark:


## Group 22

- Repository: [Submission](https://github.com/hpmouton/Distributed-Systems-assignment.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Vetara Kaurimuje | 219116458 | FT |
| Vemuna Kaurimuje | 219048355 | FT |
| Hubert Mouton | 219079714 | FT |
| Maria Thomas | 219063060 | FT |


- Presentation
	- Date: 11/02/2021
	- Time: 16:00 - 16:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 22/11/2020
			- all members contributed: yes
		
		
		- remote interface
			- addRecord, update, read (with key, key and version)
		- gRPC client
			- no comment
		- gRPC server
			- the server weirdly is never reactive to any remote action from the server
		- Server-side logic
			- versioning: the client manages the versions
			- persistence: not implemented
		- running implementation
			- partially (strangely the communication between the client and the server is subject to questions)
- Mark:


## Group 23

- Repository: [Submission](https://gitlab.com/dsp-assignment/project-1.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Lazarus Matheus | 219067023 |  |
| Basilius Shivute | 219060754 |  |
| Selma Auala | 219009430 |  |
| Christoph Iitope | 217139485 |  |


- Presentation
	- Date:
	- Time:
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- remote interface
			- create, update, read (not quite clear)
		- gRPC client
			- nothing noteworthy
		- gRPC server
			- nothing noteworthy
		- Server-side logic
			- versioning not implemented
			- persistence: store objects in a txt file (a bit confusing)
		- running program
			- no
- Mark:


## Group 24

- Repository: [Submission](https://github.com/218064187/cali.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Stephen Shilongo | 218059299 | PT |
| Bill Tonata | 218099509 | PT |
| Hango Ndeyapo | 218064187 | PT |


- Presentation
	- Date: 11/12/2020
	- Time: 11:00 - 11:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 21/11/2020
			- all members contributed: no (only one)
		
		
		- remote interface
			- write function
			- update function
			- read with key
			- read with criteria (?)
			- the objects in these functions are sometimes weird
		- gRPC client
			- the client implements all functions from the proto file
			- no streaming is showing
			- the use cases are hardcoded
		- gRPC server
			- the server implements all the functions in the client, but does not do streaming for the read with criteria
		- server-side logic
			- the records are stored in an in-memory map: no persistence of records
			- the version numbers are stored in the map, no proper implementation of the versions
		- running program
			- yes
- Mark:

## Group 25

- Repository: [Submission](https://gitlab.com/consuelo.ogbokor/dsp-assignment-1/-/tree/master)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Elo Gloria Ogbokor | 216062195 | FT |

- Presentation
	- Date: 04/2/2021
	- Time: 18:20 - 18:40
	- Comments:
		- repository
			- accessible: yes
			- latest commit:15/11/2020
			- all members contributed: yes zipped file uploaded
		
		
		- remote interface
			- recordon function
			- recordexist function
            - notexist function
            - modify function
            - update function
		- gRPC client
			- There is barely nothing implemented because all data are hard coded
		- gRPC server
			- Search data on the hard coded data is implemented
		- Server-side logic
		  	- hardcoded data is being send to a json file
		- running program
		 - yes
- Mark:


## Group 26

- Repository: [Submission](https://github.com/Marvel-1001/DSP_Ass.1)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Gabriel-Marvel Mwandi | 218111010 | FT |
| Sharon Boois | 219116008 | FT |
| Eraastus Ugulu | 219123349 | FT |

- Presentation
	- Date:
	- Time:
	- Comments:
		- repository
			- accessible: yes
			- latest commit:
			- all members contributed:
		
		
		- remote interface
			- write
			- update
			- read (with key)
		- gRPC client
			- attempted to implement gRPC
		- gRPC server
			- attempted to implement gRPC
		- server-side logic
			- no persistence; records are only stored in memory
			- versioning not implemented
			- tried CAS but not fully
		- running program
			- No
- Mark:

## Group 27

- Repository: [Submission](https://github.com/Ndeumona/DSP-Assignment-1)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Tabitha Nehale | 200840851 | FT |
| Shelma Shikongo | 218000146 | FT |
| Uajaa Veii | 200529994 | FT |
| Ndapandula Shiweva | 218072422 | FT |

- Presentation
	- Date: 11/02/2021
	- Time: 17:20 - 17:40
	- Comments:
		- repository
			- accessible: yes
			- latest commit:
			- all members contributed:
		
		
		- remote interface
			- add, update, read (with key, key and version)
		- gRPC client
			- according to the remote interface
		- gRPC server
			- according to the remote interface
		- Server-side logic
			- versioning: implemented versioning with copy of the old version
			- persisted objects in a json file, although one file per object
		- running program
			- yes
- Mark:


## Group 28

- Repository: [Submission](https://gitlab.com/grouppros2020/dsp-assignment/-/tree/master/assignment_1)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Eugene Mwewa | 218040245| FT |
| Hena Sibalatani| 219153566 | FT |
| melda Haufiku | 219065128 | FT |

- Presentation
	- Date: 12/02/2021
	- Time: 11:00 - 11:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit:15/11/2020
			- all members contributed:yes
		
		
		- remote interface
			- writeRecord function
			- update record function
			- View Record Function
		- gRPC client
			- implemented with hardcoded data
		- gRPC server
			- hashing function is implemented
			- only the write record is fully implemented with hardcoded data
		- Server-side logic
			- data is being stored to an array
		- running program
			- yes
- Mark:



## Group 29

- Repository: [Submission](https://github.com/hpmouton/Distributed-Systems-assignment.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Vetara Kaurimuje  | 219116458 | FT |
| Vemuna Kaurimuje | 219048355 | FT |
| Maria Thomasa | 219063060 | FT |
| Hubert Mouton |  219079714 | FT |

- Presentation
	- Date: 12/02/2021
	- Time: 10:40 - 11:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit:
			- all members contributed:
		
		
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
			- comment
		- running program
			- no/yes
- Mark:


## Group 30

- Repository: [Submission](https://github.com/braulio2020/dsp_assignment1)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Braulio Andre | 219036810 | |
| Clive Mafwila | 217048269 | |


- Presentation
	- Date: 12/02/2021
	- Time: 11:40 - 12:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit:
			- all members contributed:
		
		
		- remote interface
			- write, update, read (with key)
		- gRPC client
			- could not tell
		- gRPC server
			- could not tell
		- Server-side logic
			- versioning not implemented
			- persistence not implemented
		- running program
			- partially
			- the students do not seem to know how to get the client and the server to communicate
- Mark:

## Group 31

- Repository: [Submission](https://github.com/Ashivudhi/DSP620S-Assignment-1.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| David Ingashipola | 219036810 | |
| Ashivudhi Erro REmainder | 217048269 | |
| Valombola Simon | 219036810 | |


- Presentation
	- Date: 12/02/2021
	- Time: 12:20 - 12:40
	- Comments:
		- repository
			- accessible: yes
			- latest commit:22/11/2020
			- all members contributed:yes
		
		
		- remote interface
			- writeRecord function
			- updateRecord function
			- readRecord function
			- readRecord_Key_Version function
			- readRecord_Criterion function
		- gRPC client
			- All the functions are implemented except the readRecord with criteria function
			- data are hardcoded
		- gRPC server
			- hashing function is implemented
			- data is being saved to a json file
		- Server-side logic
			- versioning not implemented
		- running program
			- yes
- Mark:

## Group 32

- Repository: [Submission](https://github.com/josemarlima/DSP-ASSIGNMNET)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
|  U Hange | 200512579 | |
|  J Lima  | 219011397| |
|  R Junias | 219041466 | |


- Presentation
	- Date: 12/02/2021
	- Time: 09:20 - 09:40
	- Comments:
		- repository
			- accessible: yes
			- latest commit:22/11/2020
			- all members contributed:yes 
		
		
		- remote interface
			- saveRecord function
			- changeRecord function
			- displayRecord function
		- gRPC client
			- implemennted but nothing much happening. only displaying the menu
		- gRPC server
			- Implemented but only saveRecord function is implemented
		- Server-side logic
			- no versioning
			- no hashing function 
		- running program
			- no due to the updated version
- Mark:

## Group 33

- Repository: [Submission](https://github.com/ScientistFromIcarus/Assignment1)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
|  Amelia kenya | 219067627 | |
|  Fillemon Frans | 219000131| |
|  Queen Imbili | 219152950 | |


- Presentation
	- Date: 15/02/2021
	- Time: 10:00 - 10:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit:
			- all members contributed:
		
		
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
			- comment
		- running program
			- no/yes
- Mark:


## Group 34

- Repository: [Submission](https://github.com/WitnessA/Assignment1)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
|  Witness Asino | 217067247 | |


- Presentation
	- Date: 12/02/2021
	- Time: 12:40 - 13:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
			- all members contributed:
		
		
		- remote interface
			- writeRecord, readRecord (with criteria but no streaming)
		- gRPC client
			- not working
		- gRPC server
			- not working (the client and the server cannot communicate)
		- Server-side logic
			- versioning: not implemented
			- persistence: store an array of json objects in a file
		- running program
			- no
- Mark:
