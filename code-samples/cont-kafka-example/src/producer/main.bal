import ballerina/io;
import ballerina/kafka;
import ballerina/log;
import ballerina/docker;

kafka:ProducerConfiguration prodConf = {
	bootstrapServers: "localhost:9092",
	clientId: "basic-producer",
	acks: "all",
	retryCount: 3,
	valueSerializerType: kafka:SER_STRING,
	keySerializerType: kafka:SER_INT
};

kafka:Producer prod = new (prodConf);

@docker:Config {
	name: "cproducer",
	tag: "v1.0"
}

public function main() {
    io:println("Welcome to the Kafka tutorial with containerisation...");
	
	json msg = {
		assignmentDeadline: "29/01/2021",
		submissionMode: "git-repo",
		presentation: "TBD"
	};
	
	//byte[] serialisedMsg = msg.toString().toBytes();
	var sendRes = prod -> send(msg.toString(), "dsp", partition = 0);

	if(sendRes is error)
	{
		log:printError("Error occurred while committing the " +
                "offsets for the consumer ", sendRes);
	}
}
